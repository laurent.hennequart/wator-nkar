# WATOR
## I. PredWator
Objectif : assimiler la notion de tore et sa représentation plane et introduire la notion de déplacement sur cette représentation.

### 1. Notion de tore et représentation planaire
#### a) Globe et planisphère
Faire le lien entre deux représentations, l'une réelle l'autre plane.

Sur [wikipedia](https://fr.wikipedia.org/wiki/Planisphère)

**Combien de coordonnées nécessaires pour une position sur le globe ?**

Passage aux pôles, passage au méridien 0 (impact sur les coordonnées).
#### b) Tore et planisphère
Visualisation d'un tore : utilisation de [ThreeJS](https://threejs.org/docs/index.html#api/en/geometries/TorusGeometry).

**Combien de coordonnées nécessaires pour une position sur le tore ?**

Association à une représentation rectangulaire : passage à Python, création d'une grille représentant la mer torique.

Python : **crée une fontion** `genere_mer_vide(n,m)` simulant une mer torique vide, les paramètres n et m représantant respectivement 
la longueur du tore (associée à son rayon) et la longueur du tube (associée au rayon du tube). Il s'agira pour cela d'associer la représentation rectangulaire du tore à une grille nxm contenant des 0.

### 2. Déplacement Thon-Requin sur un tore
#### a) Création de la mer initiale
Pour générer un Thon, on utilise la fonction suivante :

```python
def genere_thon():
    """
    crée une liste dans laquelle on trouve la chaine T 
    """
    return ['T']
```

**Crée** une fonction `genere_requin()`.

**Teste** dans la console la fonction suivante et **documente-la** :

```python
def genere_grille_initiale(long,haut):
    mer=genere_mer_vide(long,haut)
    for i in range(len(mer)):
        for j in range(len(mer[0])):
            mer[i][j]=[0]
    mer[0][randint(1,long-1)]=genere_thon()
    mer[randint(1,haut-1)][0]=genere_requin()
    return mer
```

**Crée** ensuite une fonction `affiche_mer(grille)` permettant de visualiser la mer torique sous la forme d'un rectangle où toutes les positions sont représentées par le caractère _ et le thon par T et le requin par R.

#### b) Déplacement du thon et du requin
A partir de la grille initiale, on va simuler le déplacement d'un thon (du haut vers le bas) et celui d'un requin (de la gauche vers la droite).
Pour cela utilise la fonction suivante et **complète la** sachant que le thon doit se déplacer d'une case **vers le bas** et le requin d'une case **vers la droite**  : 

```python
def etape_suivante(grille):
    mer=genere_mer_vide(len(grille[0]),len(grille))
    for i in range(len(grille)):
        for j in range(len(grille[0])):
            mer[i][j]=[0]
    for i in range(len(grille)):
        for j in range(len(grille[0])):
            if grille[i][j][0]=="T":
                # code ici
            elif grille[i][j][0]=="R":
                # code ici
    return mer
```
**Complète ensuite ton programme** avec une fonction `evolution_n_generations(long,haut,n)` permettant d'afficher les différents 
déplacements des deux bêtes en utilisant la fonction `affiche_mer(grille)`.

Suivant les valeurs `long` et `haut`, le thon et le requin se croisent-ils à un moment donné ?

## II. Développement du projet
Dans un contexte similaire, nous considérons des thons et des requins sur un monde torique donc représentables sur une grille identique à celle vue dans la première partie.

Les thons comme les requins peuvent se déplacer aléatoirement à chaque étape dans les quatre directions : Nord, Sud, Est et Ouest. 

Les thons, immortels, ont un temps de gestation de 2 étapes : au bout de 2 étapes, s'il peut se déplacer sur une case adjacente, il donne naissance à un nouveau thon dans la case qu'il quitte, sinon il ne bouge pas et son temps de gestation est réinitialisé.

Les requins ont un temps de gestation de 4 étapes, sur le même principe que les thons. Par contre, ils ont une durée de vie de 3 étapes, s'ils ne se déplacent pas sur un thon, ils perdent un en vie et disparaissent si cette duréee de vie arrive à zéro. S'ils se déplacent sur un thon, celui ci disparait et le requin retrouve une durée de vie de 3 étapes.


### 1. Initialisation de la grille réprésentant la mer
**Reprends la fontion** `genere_mer_vide(n,m)` codée dans la première partie.
Dans les fonctions `genere_thon()` et `genere_requin()`, on ajoute les éléments vus dans le contexte proposé en retournant `['T',2]` et `['R',4,3]`.

**Complète le code** suivant permettant de créer une première grille avec en plus les paramètres pT et pR, nombres désignant les probabilités d'avoir un thon et un requin respectivement.

On pourra dissocier les cas où on choisit un nombre `valeat` aléatoire entre 0 et 1 qu'il soit entre 0 et pT, pT et pT+pR, pT+pR et 1. 

```python
def genere_grille_initiale(long,haut,pT,pR):
    mer=genere_mer_vide(long,haut)
    for i in range(len(mer)):
        for j in range(len(mer[0])):
            valeat=random()
            if valeat>(pT+pR):
                mer[i][j]=
            elif valeat>pT:
                mer[i][j]=
            else:
                mer[i][j]=
    return mer
```

### 2. Actualisation des poissons
Pour passer à l'étape suivante, on va coder deux fonctions. L'une va d'abord prendre en compte les déplacements des thons et éventuellement la naissance des nouveaux thons et la deuxième concernant l'évolution des requins (mouvement, bouffe ou lente marche vers la mort, naissance).

Voici celle pour les thons :

```python
def etape_suivanteT(grille):
    mer=genere_mer_vide(len(grille[0]),len(grille))
    for i in range(len(grille)):
        for j in range(len(grille[0])):
            mer[i][j]=grille[i][j]
    for i in range(len(grille)):
        for j in range(len(grille[0])):
            if grille[i][j][0]=="T":
                posVide=[]
                if grille[i%len(grille)][(j-1)%len(grille[0])][0]==0:
                    posVide.append('N')
                if grille[i%len(grille)][(j+1)%len(grille[0])][0]==0:
                    posVide.append('S')
                if grille[(i-1)%len(grille)][j%len(grille[0])][0]==0:
                    posVide.append('O')
                if grille[(i+1)%len(grille)][j%len(grille[0])][0]==0:
                    posVide.append('E')
                if len(posVide)>0:
                    pos=randint(0,len(posVide)-1)
                    etat=grille[i][j][1]
                    if posVide[pos]=='N':
                        mer[i%len(grille)][(j-1)%len(grille[0])]=['T',(etat-1)%3]
                    if posVide[pos]=='S':
                        mer[i%len(grille)][(j+1)%len(grille[0])]=['T',(etat-1)%3]
                    if posVide[pos]=='O':
                        mer[(i-1)%len(grille)][j%len(grille[0])]=['T',(etat-1)%3]
                    if posVide[pos]=='E':
                        mer[(i+1)%len(grille)][j%len(grille[0])]=['T',(etat-1)%3]
                    if etat-1==0:
                        mer[i][j]=['T',2]
                    else:
                        mer[i][j]=[0]
                else:
                    etat=grille[i][j][1]
                    mer[i][j]=['T',(etat-1)%3]
    return mer
```

**Code** la fonction `etape_suivanteR(grille)` concernant les requins en prenant en compte leurs spécificités. 
### 3. Affichage de la grille
En te basant sur la technique adoptée à la première partie, affiche les etats successifs de la mer torique (utilise différentes valeurs en prenant des dimensions du tore suffisantes et des probabilités pT et pR conformes aux modèle probabiliste (dans les faits, on se prendra pT = 0.3 et pR = 0.1)).

On aura ainsi une fonction `evolution_n_generations(long,haut,pT,pR,n)`.

### 4. Evolution des deux populations
**Code** une fonction `compte_fish(grille)` qui compte le nombre de thons et de requins dans une grille représentant la mer torique. Ces valeurs seront stockées dans un tableau, et la fonction renverra par exemple `[34,12]`.

**Code** ensuite une fonction `evolution_compte(n)` qui récupère à l'aide de la fonction précédente dans un tableau les différentes valeurs de `n` états successifs d'une mer torique.

Teste ensuite tes fonctions avec l'affichage des graphiques en utlisant par exemple le module tkinter :

```python
def trace_evolution(n):
    L=evolution_compte(n)
    maFenetre = Tk()
    can1 = Canvas(maFenetre, bg='white', height=650, width=1000)
    can1.grid(row =0, column =0)
    for i in range(len(L)-1):
        can1.create_line(i,649-L[i][0],(i+1),649-L[i+1][0],width=2,fill='blue')
    for i in range(len(L)-1):
        can1.create_line(i,649-L[i][1],(i+1),649-L[i+1][1],width=2,fill='red') 
    maFenetre.mainloop()
```

Tu testeras ton programme pour `n` assez grands (100,1000,...).

**Décris l'évolution des populations des deux espèces.**